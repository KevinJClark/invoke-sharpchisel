# Invoke-SharpChisel

Powershell script to load SharpChisel which loads the embeded Golang Chisel binary -- client only to save space

An Empire module and plugin are also included for fun. Drop files in the right directories to use with Empire

Check out the original Chisel project (https://github.com/jpillora/chisel) and the SharpChisel variant (https://github.com/shantanu561993/SharpChisel)

Make sure to put your compiled chisel binary in the data/misc directory, calling it chiselserver_mac or chiselserver_linux depending on your platform

![chisel](/uploads/bec55be067a66004d99e9a2c90c85bf6/chisel.png)

![image](/uploads/da7eccc954015844dc1fadf89fbad348/image.png)